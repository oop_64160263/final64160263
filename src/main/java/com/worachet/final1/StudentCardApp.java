package com.worachet.final1;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.*;

public class StudentCardApp extends JFrame {
    JTextField txtName,txtId,txtGender;
    JLabel lblName,lblId,lblGender,lblNametxt1,lblNametxt2,lblIdtxt1,lblIdtxt2,lblGendertxt1,lblGendertxt2;
    JButton btnConfirm;
    LinkedList<String> current;

        public StudentCardApp(){
        super("Student Card App");
        current = new LinkedList();

        this.setSize(400,300);
        txtName = new JTextField();
        txtName.setBounds(90, 10, 150,20);
        txtId = new JTextField();
        txtId.setBounds(90, 40, 150,20);
        txtGender = new JTextField();
        txtGender.setBounds(90, 70, 150,20);

        btnConfirm = new JButton("Confirm");
        btnConfirm.setBounds(250,70,100,20);
        btnConfirm.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                addInfomation();
                createCard();
            }
            
        });

        

        lblName = new JLabel("?");
        lblName.setHorizontalAlignment(JLabel.CENTER);
        lblName.setFont(new Font("Calibri",Font.PLAIN,20));
        lblName.setBounds(80, 90, 200, 50);
        lblId = new JLabel("?");
        lblId.setHorizontalAlignment(JLabel.CENTER);
        lblId.setFont(new Font("Calibri",Font.PLAIN,20));
        lblId.setBounds(80, 120, 200, 50);
        lblGender = new JLabel("?");
        lblGender.setHorizontalAlignment(JLabel.CENTER);
        lblGender.setFont(new Font("Calibri",Font.PLAIN,20));
        lblGender.setBounds(80, 150, 200, 50);
        lblNametxt1 = new JLabel("ชื่อ-นามสกุล");
        lblNametxt1.setBounds(10, 10, 120, 20);
        lblIdtxt1 = new JLabel("รหัสประจำตัว");
        lblIdtxt1.setBounds(10, 40, 120, 20);
        lblGendertxt1 = new JLabel("เพศ");
        lblGendertxt1.setBounds(10, 70, 120, 20);
        lblNametxt2 = new JLabel("ชื่อ-นามสกุล");
        lblNametxt2.setBounds(10, 90, 120, 20);
        lblIdtxt2 = new JLabel("รหัสประจำตัว");
        lblIdtxt2.setBounds(10, 120, 120, 20);
        lblGendertxt2 = new JLabel("เพศ");
        lblGendertxt2.setBounds(10, 150, 120, 20);



        this.add(txtName);
        this.add(txtId);
        this.add(txtGender);
        this.add(btnConfirm);
        this.add(lblName);
        this.add(lblId);
        this.add(lblGender);
        this.add(lblNametxt1);
        this.add(lblIdtxt1);
        this.add(lblGendertxt1);
        this.add(lblNametxt2);
        this.add(lblIdtxt2);
        this.add(lblGendertxt2);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public void addInfomation(){
        String name = txtName.getText();
        current.add(name);
        txtName.setText(" ");
        String id = txtId.getText();
        current.add(id);
        txtId.setText(" ");
        String gender = txtGender.getText();
        current.add(gender);
        txtGender.setText(" ");
    }
    public void createCard(){
        String name =current.remove();
        lblName.setText(name);
        String id =current.remove();
        lblId.setText(id);
        String gender =current.remove();
        lblGender.setText(gender);
    }

    public static void main(String[] args) {
        StudentCardApp frame = new StudentCardApp();
    }
}
